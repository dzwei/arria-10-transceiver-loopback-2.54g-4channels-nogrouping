--if you want to modify channel
--just modify component of transceiver(xcvr)

--!! warning !!
--user need to see "DataStruct_param_def_header.vhd"
--most important of typedef and parameter in here
--!! warning !!

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

library work;
use work.DataStruct_param_def_header.all;--invoke our defined type and parameter

entity XCVR_8B10B_interconnect is
    port (
        RST_N                       : in  std_logic := '0' ;

        TX_para_external_ch         : in  para_data_men;
        RX_para_external_ch         : out para_data_men;
        TX_para_external_clk_ch     : out ser_data_men;
        RX_para_external_clk_ch     : out ser_data_men;
        tx_traffic_ready_ext_ch     : out ser_data_men;
        rx_traffic_ready_ext_ch     : out ser_data_men;
        error_cnt_ch                : out para_data_men;

        Ref_Clock                   : in  std_logic := '0';

        RX_ser                      : in  ser_data_men;
        TX_ser                      : out ser_data_men
    );
end entity XCVR_8B10B_interconnect ;


architecture Top of XCVR_8B10B_interconnect is
    --clock
    signal XCVR_Tx_clk_out_ch     : ser_data_men := (others =>'0');
    signal XCVR_Rx_clk_out_ch     : ser_data_men := (others =>'0');
    --==================--
    ----data gen/check----
    --==================--
    --loopback_en
    signal internal_loopback_en_ch       : ser_data_men := (others =>'0');

    --para data
    signal tx_Para_data_ch               : para_data_men;
    signal rx_Para_data_ch               : para_data_men;

    signal tx_Para_data_external_buf_ch  : para_data_men;
    signal rx_Para_data_external_buf_ch  : para_data_men;

    signal tx_Para_data_internal_buf_ch  : para_data_men;
    signal rx_Para_data_internal_buf_ch  : para_data_men;

    signal tx_Para_data_internal_ch      : para_data_men;
    signal rx_Para_data_internal_ch      : para_data_men;

    signal xcvr_tx_Para_data_ch          : para_data_men;
    signal xcvr_rx_Para_data_ch          : para_data_men;

    --ready

    signal TX_gen_ready_ch          : ser_data_men := (others =>'0');
    signal RX_chk_ready_ch          : ser_data_men := (others =>'0');
    signal TX_traffic_ready_ch      : ser_data_men := (others =>'0');
    signal RX_traffic_ready_ch      : ser_data_men := (others =>'0');

    signal tx_gen_ready_internal_ch      : ser_data_men := (others =>'0');
    signal rx_chk_ready_internal_ch      : ser_data_men := (others =>'0');
    signal tx_traffic_ready_internal_ch  : ser_data_men := (others =>'0');
    signal rx_traffic_ready_internal_ch  : ser_data_men := (others =>'0');

    signal XCVR_TX_ready_ch      : ser_data_men := (others =>'0'); --arria10
    signal XCVR_RX_ready_ch      : ser_data_men := (others =>'0'); --arria10
    --===============--
    ----transceiver----
    --===============--

    --ser data
    signal Tx_ser_buf               : ser_data_men := (others =>'0');
    signal Rx_ser_buf               : ser_data_men := (others =>'0');

    --controlled data
    signal rx_disp_err_ch           : ctrl_code_8B10B;
    signal rx_err_detec_ch          : ctrl_code_8B10B;

    signal rx_data_k_ch             : ctrl_code_8B10B;
    signal tx_data_k_ch             : ctrl_code_8B10B;

    signal rx_patterndetect_ch      : ctrl_code_8B10B; --arria10
    signal rx_syncstatus_ch         : ctrl_code_8B10B; --arria10
    signal rx_runningdisp_ch        : ctrl_code_8B10B; --arria10

    signal rx_std_wa_patternalign   : ser_data_men := (others =>'0');--arria10
    --===============--
    --for Stratix-4  --
    --===============--

    --Xcvr
    signal XCVR_TxRx_rst            : ser_data_men := (others =>'0');

    signal rx_enapatternalign       : ser_data_men := (others =>'0');

    signal rx_freqlocked_ch         : ser_data_men := (others =>'0');--arria 10
    component xcvr_8B10B is
		port (
			clk_clk                                                             : in  std_logic                      := 'X';             -- clk
			reset_reset_n                                                       : in  std_logic                      := 'X';             -- reset_n
			xcvr_native_a10_0_rx_clkout_ch0_clk                                 : out std_logic;                                         -- clk
			xcvr_native_a10_0_rx_clkout_ch1_clk                                 : out std_logic;                                         -- clk
			xcvr_native_a10_0_rx_clkout_ch2_clk                                 : out std_logic;                                         -- clk
			xcvr_native_a10_0_rx_clkout_ch3_clk                                 : out std_logic;                                         -- clk
			xcvr_native_a10_0_rx_coreclkin_ch0_clk                              : in  std_logic                      := 'X';             -- clk
			xcvr_native_a10_0_rx_coreclkin_ch1_clk                              : in  std_logic                      := 'X';             -- clk
			xcvr_native_a10_0_rx_coreclkin_ch2_clk                              : in  std_logic                      := 'X';             -- clk
			xcvr_native_a10_0_rx_coreclkin_ch3_clk                              : in  std_logic                      := 'X';             -- clk
			xcvr_native_a10_0_rx_datak_ch0_rx_datak                             : out std_logic_vector(1 downto 0);                      -- rx_datak
			xcvr_native_a10_0_rx_datak_ch1_rx_datak                             : out std_logic_vector(1 downto 0);                      -- rx_datak
			xcvr_native_a10_0_rx_datak_ch2_rx_datak                             : out std_logic_vector(1 downto 0);                      -- rx_datak
			xcvr_native_a10_0_rx_datak_ch3_rx_datak                             : out std_logic_vector(1 downto 0);                      -- rx_datak
			xcvr_native_a10_0_rx_disperr_ch0_rx_disperr                         : out std_logic_vector(1 downto 0);                      -- rx_disperr
			xcvr_native_a10_0_rx_disperr_ch1_rx_disperr                         : out std_logic_vector(1 downto 0);                      -- rx_disperr
			xcvr_native_a10_0_rx_disperr_ch2_rx_disperr                         : out std_logic_vector(1 downto 0);                      -- rx_disperr
			xcvr_native_a10_0_rx_disperr_ch3_rx_disperr                         : out std_logic_vector(1 downto 0);                      -- rx_disperr
			xcvr_native_a10_0_rx_errdetect_ch0_rx_errdetect                     : out std_logic_vector(1 downto 0);                      -- rx_errdetect
			xcvr_native_a10_0_rx_errdetect_ch1_rx_errdetect                     : out std_logic_vector(1 downto 0);                      -- rx_errdetect
			xcvr_native_a10_0_rx_errdetect_ch2_rx_errdetect                     : out std_logic_vector(1 downto 0);                      -- rx_errdetect
			xcvr_native_a10_0_rx_errdetect_ch3_rx_errdetect                     : out std_logic_vector(1 downto 0);                      -- rx_errdetect
			xcvr_native_a10_0_rx_is_lockedtoref_ch0_rx_is_lockedtoref           : out std_logic;                                         -- rx_is_lockedtoref
			xcvr_native_a10_0_rx_is_lockedtoref_ch1_rx_is_lockedtoref           : out std_logic;                                         -- rx_is_lockedtoref
			xcvr_native_a10_0_rx_is_lockedtoref_ch2_rx_is_lockedtoref           : out std_logic;                                         -- rx_is_lockedtoref
			xcvr_native_a10_0_rx_is_lockedtoref_ch3_rx_is_lockedtoref           : out std_logic;                                         -- rx_is_lockedtoref
			xcvr_native_a10_0_rx_parallel_data_ch0_rx_parallel_data             : out std_logic_vector(15 downto 0);                     -- rx_parallel_data
			xcvr_native_a10_0_rx_parallel_data_ch1_rx_parallel_data             : out std_logic_vector(15 downto 0);                     -- rx_parallel_data
			xcvr_native_a10_0_rx_parallel_data_ch2_rx_parallel_data             : out std_logic_vector(15 downto 0);                     -- rx_parallel_data
			xcvr_native_a10_0_rx_parallel_data_ch3_rx_parallel_data             : out std_logic_vector(15 downto 0);                     -- rx_parallel_data
			xcvr_native_a10_0_rx_patterndetect_ch0_rx_patterndetect             : out std_logic_vector(1 downto 0);                      -- rx_patterndetect
			xcvr_native_a10_0_rx_patterndetect_ch1_rx_patterndetect             : out std_logic_vector(1 downto 0);                      -- rx_patterndetect
			xcvr_native_a10_0_rx_patterndetect_ch2_rx_patterndetect             : out std_logic_vector(1 downto 0);                      -- rx_patterndetect
			xcvr_native_a10_0_rx_patterndetect_ch3_rx_patterndetect             : out std_logic_vector(1 downto 0);                      -- rx_patterndetect
			xcvr_native_a10_0_rx_runningdisp_ch0_rx_runningdisp                 : out std_logic_vector(1 downto 0);                      -- rx_runningdisp
			xcvr_native_a10_0_rx_runningdisp_ch1_rx_runningdisp                 : out std_logic_vector(1 downto 0);                      -- rx_runningdisp
			xcvr_native_a10_0_rx_runningdisp_ch2_rx_runningdisp                 : out std_logic_vector(1 downto 0);                      -- rx_runningdisp
			xcvr_native_a10_0_rx_runningdisp_ch3_rx_runningdisp                 : out std_logic_vector(1 downto 0);                      -- rx_runningdisp
			xcvr_native_a10_0_rx_serial_data_ch0_rx_serial_data                 : in  std_logic                      := 'X';             -- rx_serial_data
			xcvr_native_a10_0_rx_serial_data_ch1_rx_serial_data                 : in  std_logic                      := 'X';             -- rx_serial_data
			xcvr_native_a10_0_rx_serial_data_ch2_rx_serial_data                 : in  std_logic                      := 'X';             -- rx_serial_data
			xcvr_native_a10_0_rx_serial_data_ch3_rx_serial_data                 : in  std_logic                      := 'X';             -- rx_serial_data
			xcvr_native_a10_0_rx_seriallpbken_ch0_rx_seriallpbken               : in  std_logic                      := 'X';             -- rx_seriallpbken
			xcvr_native_a10_0_rx_seriallpbken_ch1_rx_seriallpbken               : in  std_logic                      := 'X';             -- rx_seriallpbken
			xcvr_native_a10_0_rx_seriallpbken_ch2_rx_seriallpbken               : in  std_logic                      := 'X';             -- rx_seriallpbken
			xcvr_native_a10_0_rx_seriallpbken_ch3_rx_seriallpbken               : in  std_logic                      := 'X';             -- rx_seriallpbken
			xcvr_native_a10_0_rx_std_wa_patternalign_ch0_rx_std_wa_patternalign : in  std_logic                      := 'X';             -- rx_std_wa_patternalign
			xcvr_native_a10_0_rx_std_wa_patternalign_ch1_rx_std_wa_patternalign : in  std_logic                      := 'X';             -- rx_std_wa_patternalign
			xcvr_native_a10_0_rx_std_wa_patternalign_ch2_rx_std_wa_patternalign : in  std_logic                      := 'X';             -- rx_std_wa_patternalign
			xcvr_native_a10_0_rx_std_wa_patternalign_ch3_rx_std_wa_patternalign : in  std_logic                      := 'X';             -- rx_std_wa_patternalign
			xcvr_native_a10_0_rx_syncstatus_ch0_rx_syncstatus                   : out std_logic_vector(1 downto 0);                      -- rx_syncstatus
			xcvr_native_a10_0_rx_syncstatus_ch1_rx_syncstatus                   : out std_logic_vector(1 downto 0);                      -- rx_syncstatus
			xcvr_native_a10_0_rx_syncstatus_ch2_rx_syncstatus                   : out std_logic_vector(1 downto 0);                      -- rx_syncstatus
			xcvr_native_a10_0_rx_syncstatus_ch3_rx_syncstatus                   : out std_logic_vector(1 downto 0);                      -- rx_syncstatus
			xcvr_native_a10_0_tx_clkout_ch0_clk                                 : out std_logic;                                         -- clk
			xcvr_native_a10_0_tx_clkout_ch1_clk                                 : out std_logic;                                         -- clk
			xcvr_native_a10_0_tx_clkout_ch2_clk                                 : out std_logic;                                         -- clk
			xcvr_native_a10_0_tx_clkout_ch3_clk                                 : out std_logic;                                         -- clk
			xcvr_native_a10_0_tx_coreclkin_ch0_clk                              : in  std_logic                      := 'X';             -- clk
			xcvr_native_a10_0_tx_coreclkin_ch1_clk                              : in  std_logic                      := 'X';             -- clk
			xcvr_native_a10_0_tx_coreclkin_ch2_clk                              : in  std_logic                      := 'X';             -- clk
			xcvr_native_a10_0_tx_coreclkin_ch3_clk                              : in  std_logic                      := 'X';             -- clk
			xcvr_native_a10_0_tx_datak_ch0_tx_datak                             : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- tx_datak
			xcvr_native_a10_0_tx_datak_ch1_tx_datak                             : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- tx_datak
			xcvr_native_a10_0_tx_datak_ch2_tx_datak                             : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- tx_datak
			xcvr_native_a10_0_tx_datak_ch3_tx_datak                             : in  std_logic_vector(1 downto 0)   := (others => 'X'); -- tx_datak
			xcvr_native_a10_0_tx_parallel_data_ch0_tx_parallel_data             : in  std_logic_vector(15 downto 0)  := (others => 'X'); -- tx_parallel_data
			xcvr_native_a10_0_tx_parallel_data_ch1_tx_parallel_data             : in  std_logic_vector(15 downto 0)  := (others => 'X'); -- tx_parallel_data
			xcvr_native_a10_0_tx_parallel_data_ch2_tx_parallel_data             : in  std_logic_vector(15 downto 0)  := (others => 'X'); -- tx_parallel_data
			xcvr_native_a10_0_tx_parallel_data_ch3_tx_parallel_data             : in  std_logic_vector(15 downto 0)  := (others => 'X'); -- tx_parallel_data
			xcvr_native_a10_0_tx_serial_data_ch0_tx_serial_data                 : out std_logic;                                         -- tx_serial_data
			xcvr_native_a10_0_tx_serial_data_ch1_tx_serial_data                 : out std_logic;                                         -- tx_serial_data
			xcvr_native_a10_0_tx_serial_data_ch2_tx_serial_data                 : out std_logic;                                         -- tx_serial_data
			xcvr_native_a10_0_tx_serial_data_ch3_tx_serial_data                 : out std_logic;                                         -- tx_serial_data
			xcvr_native_a10_0_unused_rx_parallel_data_unused_rx_parallel_data   : out std_logic_vector(399 downto 0);                    -- unused_rx_parallel_data
			xcvr_native_a10_0_unused_tx_parallel_data_unused_tx_parallel_data   : in  std_logic_vector(439 downto 0) := (others => 'X'); -- unused_tx_parallel_data
			xcvr_reset_control_0_pll_select_pll_select                          : in  std_logic_vector(0 downto 0)   := (others => 'X'); -- pll_select
            xcvr_reset_control_0_reset_reset                                    : in  std_logic                      := 'X';             -- reset
            xcvr_reset_control_0_rx_ready0_rx_ready                             : out std_logic;                                         -- rx_ready
			xcvr_reset_control_0_rx_ready1_rx_ready                             : out std_logic;                                         -- rx_ready
			xcvr_reset_control_0_rx_ready2_rx_ready                             : out std_logic;                                         -- rx_ready
			xcvr_reset_control_0_rx_ready3_rx_ready                             : out std_logic;                                         -- rx_ready
			xcvr_reset_control_0_tx_ready0_tx_ready                             : out std_logic;                                         -- tx_ready
			xcvr_reset_control_0_tx_ready1_tx_ready                             : out std_logic;                                         -- tx_ready
			xcvr_reset_control_0_tx_ready2_tx_ready                             : out std_logic;                                         -- tx_ready
			xcvr_reset_control_0_tx_ready3_tx_ready                             : out std_logic                                          -- tx_ready
		);
	end component xcvr_8B10B;

begin
    --connecte ser data
    RX_ser_buf  <=  Rx_ser;
    Tx_ser      <=  Tx_ser_buf ;

    --connect loopback_en
    internal_loopback_en_ch <= (others =>'1') when xcvr_ser_internal_loopback_en = '1' else (others =>'0');

    --connecte para data with internal/external selector
    tx_Para_data_ch <= tx_Para_data_external_buf_ch when scr_para_Data_gen_check_form_this_module = '0';
    tx_Para_data_ch <= tx_Para_data_internal_buf_ch when scr_para_Data_gen_check_form_this_module = '1';

    rx_Para_data_external_buf_ch <= rx_Para_data_ch when scr_para_Data_gen_check_form_this_module = '0';
    rx_Para_data_internal_buf_ch <= rx_Para_data_ch when scr_para_Data_gen_check_form_this_module = '1';

    tx_Para_data_internal_buf_ch <= tx_Para_data_internal_ch when scr_para_Data_gen_check_form_this_module = '1';
    tx_Para_data_external_buf_ch <= TX_para_external_ch      when scr_para_Data_gen_check_form_this_module = '0';

    rx_Para_data_internal_ch <= rx_Para_data_internal_buf_ch when scr_para_Data_gen_check_form_this_module = '1';
    RX_para_external_ch      <= rx_Para_data_external_buf_ch when scr_para_Data_gen_check_form_this_module = '0';

    TX_para_external_clk_ch <= XCVR_Tx_clk_out_ch when scr_para_Data_gen_check_form_this_module = '0';
    RX_para_external_clk_ch <= XCVR_Rx_clk_out_ch when scr_para_Data_gen_check_form_this_module = '0';
    --ready-ext
    rx_traffic_ready_ext_ch <= RX_traffic_ready_ch when scr_para_Data_gen_check_form_this_module = '0';
    tx_traffic_ready_ext_ch <= TX_traffic_ready_ch when scr_para_Data_gen_check_form_this_module = '0';
    --ready-int
    rx_traffic_ready_internal_ch     <= RX_traffic_ready_ch      when scr_para_Data_gen_check_form_this_module = '1';
    tx_traffic_ready_internal_ch     <= TX_traffic_ready_ch      when scr_para_Data_gen_check_form_this_module = '1';

    --XCVR module connect
    XCVR : xcvr_8B10B
    port map(
        clk_clk                                                   => Ref_Clock,
        reset_reset_n                                             => RST_N,

        xcvr_native_a10_0_rx_clkout_ch0_clk                       => XCVR_Rx_clk_out_ch(0),
        xcvr_native_a10_0_rx_clkout_ch1_clk                       => XCVR_Rx_clk_out_ch(1),
        xcvr_native_a10_0_rx_clkout_ch2_clk                       => XCVR_Rx_clk_out_ch(2),
        xcvr_native_a10_0_rx_clkout_ch3_clk                       => XCVR_Rx_clk_out_ch(3),

        xcvr_native_a10_0_rx_coreclkin_ch0_clk                    => XCVR_Rx_clk_out_ch(0),
        xcvr_native_a10_0_rx_coreclkin_ch1_clk                    => XCVR_Rx_clk_out_ch(1),
        xcvr_native_a10_0_rx_coreclkin_ch2_clk                    => XCVR_Rx_clk_out_ch(2),
        xcvr_native_a10_0_rx_coreclkin_ch3_clk                    => XCVR_Rx_clk_out_ch(3),

        xcvr_native_a10_0_rx_datak_ch0_rx_datak                   => rx_data_k_ch(0),
        xcvr_native_a10_0_rx_datak_ch1_rx_datak                   => rx_data_k_ch(1),
        xcvr_native_a10_0_rx_datak_ch2_rx_datak                   => rx_data_k_ch(2),
        xcvr_native_a10_0_rx_datak_ch3_rx_datak                   => rx_data_k_ch(3),

        xcvr_native_a10_0_rx_disperr_ch0_rx_disperr               => rx_disp_err_ch(0),
        xcvr_native_a10_0_rx_disperr_ch1_rx_disperr               => rx_disp_err_ch(1),
        xcvr_native_a10_0_rx_disperr_ch2_rx_disperr               => rx_disp_err_ch(2),
        xcvr_native_a10_0_rx_disperr_ch3_rx_disperr               => rx_disp_err_ch(3),

        xcvr_native_a10_0_rx_errdetect_ch0_rx_errdetect           => rx_err_detec_ch(0),
        xcvr_native_a10_0_rx_errdetect_ch1_rx_errdetect           => rx_err_detec_ch(1),
        xcvr_native_a10_0_rx_errdetect_ch2_rx_errdetect           => rx_err_detec_ch(2),
        xcvr_native_a10_0_rx_errdetect_ch3_rx_errdetect           => rx_err_detec_ch(3),

        xcvr_native_a10_0_rx_is_lockedtoref_ch0_rx_is_lockedtoref => rx_freqlocked_ch(0),
        xcvr_native_a10_0_rx_is_lockedtoref_ch1_rx_is_lockedtoref => rx_freqlocked_ch(1),
        xcvr_native_a10_0_rx_is_lockedtoref_ch2_rx_is_lockedtoref => rx_freqlocked_ch(2),
        xcvr_native_a10_0_rx_is_lockedtoref_ch3_rx_is_lockedtoref => rx_freqlocked_ch(3),

        xcvr_native_a10_0_rx_parallel_data_ch0_rx_parallel_data   => xcvr_rx_Para_data_ch(0),
        xcvr_native_a10_0_rx_parallel_data_ch1_rx_parallel_data   => xcvr_rx_Para_data_ch(1),
        xcvr_native_a10_0_rx_parallel_data_ch2_rx_parallel_data   => xcvr_rx_Para_data_ch(2),
        xcvr_native_a10_0_rx_parallel_data_ch3_rx_parallel_data   => xcvr_rx_Para_data_ch(3),

        xcvr_native_a10_0_rx_patterndetect_ch0_rx_patterndetect   => rx_patterndetect_ch(0),
        xcvr_native_a10_0_rx_patterndetect_ch1_rx_patterndetect   => rx_patterndetect_ch(1),
        xcvr_native_a10_0_rx_patterndetect_ch2_rx_patterndetect   => rx_patterndetect_ch(2),
        xcvr_native_a10_0_rx_patterndetect_ch3_rx_patterndetect   => rx_patterndetect_ch(3),

        xcvr_native_a10_0_rx_runningdisp_ch0_rx_runningdisp       => rx_runningdisp_ch(0),
        xcvr_native_a10_0_rx_runningdisp_ch1_rx_runningdisp       => rx_runningdisp_ch(1),
        xcvr_native_a10_0_rx_runningdisp_ch2_rx_runningdisp       => rx_runningdisp_ch(2),
        xcvr_native_a10_0_rx_runningdisp_ch3_rx_runningdisp       => rx_runningdisp_ch(3),

        xcvr_native_a10_0_rx_serial_data_ch0_rx_serial_data       => RX_ser_buf(0),
        xcvr_native_a10_0_rx_serial_data_ch1_rx_serial_data       => RX_ser_buf(1),
        xcvr_native_a10_0_rx_serial_data_ch2_rx_serial_data       => RX_ser_buf(2),
        xcvr_native_a10_0_rx_serial_data_ch3_rx_serial_data       => RX_ser_buf(3),

        xcvr_native_a10_0_rx_seriallpbken_ch0_rx_seriallpbken     => internal_loopback_en_ch(0),
        xcvr_native_a10_0_rx_seriallpbken_ch1_rx_seriallpbken     => internal_loopback_en_ch(1),
        xcvr_native_a10_0_rx_seriallpbken_ch2_rx_seriallpbken     => internal_loopback_en_ch(2),
        xcvr_native_a10_0_rx_seriallpbken_ch3_rx_seriallpbken     => internal_loopback_en_ch(3),

		xcvr_native_a10_0_rx_std_wa_patternalign_ch0_rx_std_wa_patternalign    => rx_std_wa_patternalign(0),
		xcvr_native_a10_0_rx_std_wa_patternalign_ch1_rx_std_wa_patternalign    => rx_std_wa_patternalign(1),
		xcvr_native_a10_0_rx_std_wa_patternalign_ch2_rx_std_wa_patternalign    => rx_std_wa_patternalign(2),
		xcvr_native_a10_0_rx_std_wa_patternalign_ch3_rx_std_wa_patternalign    => rx_std_wa_patternalign(3),

        xcvr_native_a10_0_rx_syncstatus_ch0_rx_syncstatus         => rx_syncstatus_ch(0),
        xcvr_native_a10_0_rx_syncstatus_ch1_rx_syncstatus         => rx_syncstatus_ch(1),
        xcvr_native_a10_0_rx_syncstatus_ch2_rx_syncstatus         => rx_syncstatus_ch(2),
        xcvr_native_a10_0_rx_syncstatus_ch3_rx_syncstatus         => rx_syncstatus_ch(3),

        xcvr_native_a10_0_tx_clkout_ch0_clk                       => XCVR_Tx_clk_out_ch(0),
        xcvr_native_a10_0_tx_clkout_ch1_clk                       => XCVR_Tx_clk_out_ch(1),
        xcvr_native_a10_0_tx_clkout_ch2_clk                       => XCVR_Tx_clk_out_ch(2),
        xcvr_native_a10_0_tx_clkout_ch3_clk                       => XCVR_Tx_clk_out_ch(3),

        xcvr_native_a10_0_tx_coreclkin_ch0_clk                    => XCVR_Tx_clk_out_ch(0),
        xcvr_native_a10_0_tx_coreclkin_ch1_clk                    => XCVR_Tx_clk_out_ch(1),
        xcvr_native_a10_0_tx_coreclkin_ch2_clk                    => XCVR_Tx_clk_out_ch(2),
        xcvr_native_a10_0_tx_coreclkin_ch3_clk                    => XCVR_Tx_clk_out_ch(3),

        xcvr_native_a10_0_tx_datak_ch0_tx_datak                   => tx_data_k_ch(0),
        xcvr_native_a10_0_tx_datak_ch1_tx_datak                   => tx_data_k_ch(1),
        xcvr_native_a10_0_tx_datak_ch2_tx_datak                   => tx_data_k_ch(2),
        xcvr_native_a10_0_tx_datak_ch3_tx_datak                   => tx_data_k_ch(3),

        xcvr_native_a10_0_tx_parallel_data_ch0_tx_parallel_data   => xcvr_tx_Para_data_ch(0),
        xcvr_native_a10_0_tx_parallel_data_ch1_tx_parallel_data   => xcvr_tx_Para_data_ch(1),
        xcvr_native_a10_0_tx_parallel_data_ch2_tx_parallel_data   => xcvr_tx_Para_data_ch(2),
        xcvr_native_a10_0_tx_parallel_data_ch3_tx_parallel_data   => xcvr_tx_Para_data_ch(3),

        xcvr_native_a10_0_tx_serial_data_ch0_tx_serial_data       => Tx_ser_buf(0),
        xcvr_native_a10_0_tx_serial_data_ch1_tx_serial_data       => Tx_ser_buf(1),
        xcvr_native_a10_0_tx_serial_data_ch2_tx_serial_data       => Tx_ser_buf(2),
        xcvr_native_a10_0_tx_serial_data_ch3_tx_serial_data       => Tx_ser_buf(3),

        xcvr_reset_control_0_pll_select_pll_select                => (others => '0'),

        xcvr_reset_control_0_reset_reset                          => (XCVR_TxRx_rst(0) or XCVR_TxRx_rst(1) or XCVR_TxRx_rst(2) or XCVR_TxRx_rst(3) or (not RST_N)),

        xcvr_reset_control_0_rx_ready0_rx_ready                   => XCVR_RX_ready_ch(0),
        xcvr_reset_control_0_rx_ready1_rx_ready                   => XCVR_RX_ready_ch(1),
        xcvr_reset_control_0_rx_ready2_rx_ready                   => XCVR_RX_ready_ch(2),
        xcvr_reset_control_0_rx_ready3_rx_ready                   => XCVR_RX_ready_ch(3),

        xcvr_reset_control_0_tx_ready0_tx_ready                   => XCVR_TX_ready_ch(0),
        xcvr_reset_control_0_tx_ready1_tx_ready                   => XCVR_TX_ready_ch(1),
        xcvr_reset_control_0_tx_ready2_tx_ready                   => XCVR_TX_ready_ch(2),
        xcvr_reset_control_0_tx_ready3_tx_ready                   => XCVR_TX_ready_ch(3)
    );

    --others connect
    Data_gen_loop : for i in 0 to (num_of_xcvr_ch - 1) generate
        Data_gen : entity work.frame_gen
            port map(
                TX_D             => tx_Para_data_internal_ch(i),

                TX_traffic_ready => tx_traffic_ready_internal_ch(i),

                USER_CLK         => XCVR_Tx_clk_out_ch(i) ,
                SYSTEM_RESET_N   => RST_N
            );
    end generate Data_gen_loop;

    Data_check_loop : for i in 0 to (num_of_xcvr_ch - 1) generate
        Data_check : entity work.frame_check
            port map(
                RX_D              => rx_Para_data_internal_ch(i),

                RX_traffic_ready  => rx_traffic_ready_internal_ch(i),

                RX_errdetect      => rx_err_detec_ch(i),
                RX_disperr        => rx_disp_err_ch(i),
                rx_freq_locked    => rx_freqlocked_ch(i),

                ERROR_COUNT       => error_cnt_ch(i),

                USER_CLK          => XCVR_Rx_clk_out_ch(i),
                SYSTEM_RESET_N    => RST_N
            );
    end generate Data_check_loop;

    generate_traffic_loop : for i in 0 to (num_of_xcvr_ch - 1) generate
        traffic : entity work.traffic
            port map(
                Reset_n                  => RST_N,

                rx_freq_locked           => rx_freqlocked_ch(i),--arria10

                tx_traffic_ready         => TX_traffic_ready_ch(i),
                rx_traffic_ready         => RX_traffic_ready_ch(i),

                Tx_K                     => tx_data_k_ch(i),
                Rx_K                     => rx_data_k_ch(i),
                TX_DATA_Xcvr             => xcvr_tx_Para_data_ch(i),
                RX_DATA_Xcvr             => xcvr_rx_Para_data_ch(i),
                Tx_DATA_client           => tx_Para_data_ch(i),
                Rx_DATA_client           => rx_Para_data_ch(i),

                RX_errdetect             => rx_err_detec_ch(i),
                RX_disperr               => rx_disp_err_ch(i),

                XCVR_Manual_rst          => XCVR_TxRx_rst(i),--arria10

                Tx_xcvrRstIp_is_Ready    => XCVR_TX_ready_ch(i),--arria10
                Rx_xcvrRstIp_is_Ready    => XCVR_RX_ready_ch(i),--arria10

                rx_sync_status           => rx_syncstatus_ch(i),--arria10
                rx_pattern_detected      => rx_patterndetect_ch(i),--arria10

                rx_align_en              => rx_std_wa_patternalign(i),

                INIT_CLK                 => Ref_Clock,

                Tx_Clk                   => XCVR_Tx_clk_out_ch(i),
                Rx_Clk                   => XCVR_Rx_clk_out_ch(i)
            );
    end generate generate_traffic_loop;
end architecture Top;
