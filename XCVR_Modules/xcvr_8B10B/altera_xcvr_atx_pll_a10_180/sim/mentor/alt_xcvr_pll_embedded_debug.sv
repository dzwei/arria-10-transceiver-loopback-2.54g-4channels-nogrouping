// Copyright (C) 2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0std
// ALTERA_TIMESTAMP:Wed Apr 25 07:09:24 PDT 2018
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
O83qnrkUvhBTLIJrbSKJosvVlYdJLX4ZlrUGGixa1YnVbaEicqKPElN3bRFAZSjF
BZHT3v4yDq8yKYStxCQYgWCravUXRCoiL4d5O77Ph6GmL/5ME7jWyGRy4Bms90eP
anfCaRgh/mkkpioQyuCFWSB1MgxvhMA6Fh+vjZjxKgs=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2992)
bwdZReBhn6Vc8gGTk1cl8ACyE/Dtco3ZCH5VKm5LYZ7Y9jHSa45lHdUzzR4VW/DK
rQqYh7cUGRhqrP2xoSAHVAD61IirkbuFrbr+PjRuhseqfem9yg7yEkxAHimNk4wn
t2VULqkn6PmNqyUx5bXaNPuVskr6LDzJEqEi5R8lOeheqPWa9gJoCi/HqFCfzx2z
XziW8o86rLSj/DB84dLI5I+Q6CBMzlxz94QNUFSDjU2CjERzTV7107BwyRwWKDHo
abyfEdZZAJcCuO+IRPuvm0EbXTMrQHRkHlGG252YcFsNlPBAsvlj1qG7SrQ1mdHp
PvP3+XFHyWl5xuE3XOxIWhAo/J70+dVlLJSgTZZ6z5gM2gHBlViojFgYTuMkKvYo
+dIoTYfLSswOz7iH1fufooJnVR1RREszuSZzWMoWKO11xsii9KdfhXDtg4yGA7hz
eocICmIpZ3Jkb0zqAnxLNuWoyjGVma0DcEta/FRFHdAX44tQQoCv82Brv0EI7hii
BaRvn9Cjv9/tTbPKVNPrEmqefra8Ba8CT4GysaC+XGUcTAo9nNzZy3W2B5VntlBH
8oE1qQIuZQ7JT+l4KgiBYIMMsBbQYDWkI7RTXT3cPWAsXNvFo+scHZS4kG23YK6H
+Ge9TBagB6l5RB3kmMJqayShOc/AnhFvvi6WHU1Yp6vJfcZiNpxasS2+dhsCkdsR
DiNvakx9MNR+3R0OCkuBlXOa3k4WznDvTl3IKKNfQTo/UpOvI8K6WqQfDqnowrwp
aM47g1abGlhjUQ06iv0vfmfNm4TfIH4QWay+KQk7eWdNRbdCI060uNV8RMnpwbVP
lQL2vKDyC6bubtBwgKNipkuuaaFRJ6j8Y6iXbsHGVBLZk9TQnPtaC4o6EXp20oSY
E1xO1joxy93NSG+R0p94PKmh+zuit8KIytcsSgLPW2WNFUSB2kKXWNiG4xtbSGTO
z+RlyFmq4YggIZawg0YLuN1Hpl3E/8+e94Ip+AuVFdvKx5/wwflcz1NtEIf/FIoN
RPjrM7V0rtK6nx0zgfS3rbQ+71bw5MRTifTlstGhDAW1kUVtmIxYbOocKL2gBe+A
Rb/elWG/0xA2ai72YOmFIy101SWKFKbX7Yh1l+xVW0AxW6U5WVqIHe4d37btpWk2
TNuFwZFAMKezC6PWmLajBc8QlYeO+YdO6xHj/nkJAuFy1X61K6dCsJDJv2aPRgVA
4ywpkrCUfU2mkYDrKzijRmDNep5bOUSngr/deVnYTgfDbD4y4S792HAryQ8ZoqJM
HztLqgDGqWMI4ZRBGwsmFjVrquqgUUP+Auz7INZXLia5Pb6fP6HIi0uV+s+12e01
g/qhZ0URc1JUuHywmmlT3F8niRDe0pkMD9MjczyeL2W8dp+TStEio0/3lBDFooVw
nrRoKGHRJQh4AfeO+zeM2hX6gNSl/ntl/QUU2gRYzADtfk8DDShmosKpf9S7Uicq
n0KeToRo1nzk6tMlxhxfJdQYkiEvhMc03pWJv5derBQ1ccuB7T1UEMm1Cu1gbUFq
E0WgQNHGlpt8lxhsB4YlFx8Z0tqxmeDDXJpFRYZcI/4S1FEkNth62kwohFwkAjEk
lumuLUjfwaby9pWrkYJTBsE8cLWUwO1H5WRLC2mxMhmQYtL1S2ECV8I01hXQW1P5
58ELiashheZzaWUNW2VZozqye5f8uoCJD7Nf7Aw8gAX+1b0Lb3+X6zxmdtwvBDQc
OwR+3VLwYWXIZ+SvU1AWJNXUfRqJSodfg9HmNROzX8AYxKj40JDVUrA3AhLZT5CV
g1bhMm0xzwgV7kxbwGyG9QF8wyGDm0G0Bq7I9dDpiNxOzyrnBdA2ONXB3RVA2eLS
5gFemLDENGfdrey8uV6y0LcQAkEUxBERyIQ0KvNW4uHRIJmTZ1gSDzjjjWR48Sqz
F3WOofA2ALaXZ9/Ztz8/W0dcHdjPpsWKSD90ygHV3VylZaENdyfD9L8DfsSaIJDN
mzBx+ALrVr43A7UPI1eiDMrwPnwl/zkQ0q5bzmVGsFvqjKQ3IGACbfYcE8J19J4M
GH1Syd2eR7NDxuAINzkNlIYRUS+MBBDaRhJzg7QK/sWNmfCZ+mIMgZ4Wz6yAl0/B
+yl87t+1YT+MGyM3U3boyCr7mPMyZUPOv7DeMRMlC+h8S/Q0aSEOmhGuUI8CbHn0
Uc55ImB04RNbP6A6dFNxd7IKmPxWfHNBvraY6Cxg+XUIBTmc8FaHXQrkYx9BB8os
y3YEpbxLMtFb2DptPcD8x5NR+yPrxXVRkv8MGhsI9pTgyAgBysbIPRxUbeCRLuxO
YLUCkaRsJw9C6RqubN5lRylXoXDQWl4LjYVT7/fwPEWEpvXsVsjCfGSYLQO0Qm/L
oPW3WbS7S0aIsdrlCeMD8QVdhYJxhNjy8e36giLPyQWoGUXkGiJkdM418npbXpl3
mPSSeQK57cj95XSgS8fbtBpQldtbBP6er65spI0Tk8s5nJKxaAPIi2wW/mV7v8Ss
BP/oeU9GWc/b1te+grF9M6JB3aKZo1wvudrlowdjd48ytPmls2s01+Zpia9fH5GI
3s4c8dWVdvdqLGBZ2edWkaXfyKUBdoI8NM5j6+rfIP+rSshVdr/4ZC2kT6uU6RZi
jdDpIKCzMDe/0ZaHAVZBe5042jrQR6Pd+7e3gL3fK5zFxuwGuBS/Onz6Og+5B3FH
hRFCGycS0BWoXJULnXlWehq0rOlQmDB9K0aAE2TCAelJzleO/jwq+jww5e1kNBQA
DkzF3P1iel31f4haEuMbY+Xs70Q/2Lr6fYCkN48234oeF6Of1QeW0i+0PTQ00JOp
/DyJ9fYa7ubXC0+MCks52iGEIMXDjQiYzkZKmUHJOsmIOAJMCuluc6INQvvbityL
7M3QHYig7EKR3niym5/D6g2Ov3jJZhAC81MPVVPpzpQApH8G/aqcbdBwDjqERdJc
bmxrvhxISRsFCX7WTFQNqf+1L+vS8ociall0tv7cQZEg+jxG8NkDAEENq7EXfCrr
K06/9qVvAagkzYy2Q7pcoxP2j+WRjTyTckNin7iWEY+ja95A795fq7ElmRKi/QSB
+vrrMnGspYBELvUeL9HbKS5OGbih8GBhy+jrWih+q6cI40lzvpLQ8Ywq4faEP91I
IpSASVU3A9AOoMa5ddxfdkqe5FQ6OyWOft3EqZewuOO57N9M/OjVgXtRXgGkKN3i
CGqcfn07zmOPjsvu/iNNKbJKFjHExZ38GfW2ki9+u1nlHY8La8l9T6mSpDk7wwyh
300669Xvz337kvU8e4CrDbwz2UimEM58QTDEAQa/UO3y6jZp/TWF5D196yqi1Qk9
6n2vnBRzVeapqXJHRsH9yuhPSFT6+EX2IQmI/t3+UmnUbf74kZzndigVRkD73+yZ
NKGNka1ZAWSJkilxIFA3DUyx89bPRE7qUm1k2dNR0Z1U3BQ/gf2Qrz6smrYQ35WD
XoH/m2HFULW6S8KCXxCSSqEg9OaCagBDByEVtVtopMuNcP9xrKoG/3FspHw2wRWx
1PmzNOxcZ8aWfUQJgReZ17CXlOCuYeEr18mWtT1fcmiXM3sCNA3J4h+48yCTmkxt
FMiWcWNhNtiz/vZRfZyiHpA8LXedqnpyaMYcBkDYPazD+rejqPCdIEpUeolmTzBj
NaZS3LyNvAh4ms2IEWaHEi5Se0/0TRaSLPoHqf0ZNeRUG3ORQWsuturwC5b4Ob83
5WMm/YhRzXfbiDuz/js9+Ljs3j3ulk8vS/wTolG1YPGRGh+je25CFnYszZjCGJ0I
KuvA4NzPT2twGUYKLDAQcOox7oE7iHqxlPirTV71u3/QpRA6tvztOsljL/3MbhDO
smY/qMjPVMkCchA2tBVLrUSaeFYi64CcFXD9zl7XAGhJFQKS+/4+Gj+hTJvCBiuH
rHg2Tq59eQSMam1Xq2KUqvub4QQYbGmfwJeZAD5jtstFkgUH/ra8rm2QpgKmMvIY
2xTiPvT/V79dEjK22zr+5A==
`pragma protect end_protected
