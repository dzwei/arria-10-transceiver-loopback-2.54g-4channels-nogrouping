// Copyright (C) 2018 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 18.0std
// ALTERA_TIMESTAMP:Wed Apr 25 07:09:34 PDT 2018
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
ZbaGzBBVChqn71+N5owOpfJvJvfnPOiw1bG/1WX3Xnkc89HYsW0/PplaBForOT2d
e9LWW1LL4EhsSgUmHqccxr2FN3SIEHnQsvjz9tjU8Ti97t/ant26pMOmIaH/oAXS
zklWmLgO1S7AaQPOqM6Rl3wwxeKabu8FTjIWdhlsMVM=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2976)
1kc3h+dPNHBEnaMMSWaLHZLOeC0SjQSWm82v4lcdE815EiEOoXlLUWl8WTy4NIBV
ieyzf/KHPv3y1sZs9NOsyKg0nTlLLQ7RmeZBNW4BuUrKVltm7DQa1Ntgl/7H1eby
WXfYJ1voGrN6MNEDX5/OyjSub0igZIyyiPkhNOn5B+QabnsmgtSHoJIUppkCmg9M
rJGCVLVWgTRQG633PdG+qCZpY5C0UaJX+I2w3ZkNWC28McB18sW2KF+3ZCE59tXT
sJkWJSbAEEHaRoO1mR8A/ZXBHnyT7zXQAIZldrmkrrQoaO956WXWxO51RJBe6feQ
U3j6vs+VZjgEszD4oHuquuxJ48+8jrB0iA+5mG/uXYuIMkXAt/z1kOabriGAh3yI
NDdfFvEPHs6VzfP5ACooWafZPmiJljumH2XFoS6Ppyrb/4pbCx5hTRz/a84hmhhv
NvfojKx3tOtiqIQ1/eUa3Xy5xZ7qXT5siH8DfsnBuAZP01dquqknJ44yVRhAAtmx
K1EtYXtC49001TeeMMdW/L0dy1vsw/c5cWW8yzjZuSN+b1Xt1N1vfS3LVJM1LHgF
iGQFrKIeQglyQRCFlpCFsyrEEtOREwT94S2tXtWPy2He/BaQFTERDIkqyjqbRM8b
aqlHTVVUfJr+Of6r0fsWNjL20ZwMzhzTaBVraDRd+3je2tdXD4eCKM8b+Mzr/GgO
yePdrcSNQw7IJF9wXqDNkW02x0MgYfbqZG+RAJzGZRHAJptF6O4NG+ZW3nhd9XJw
kc2cAxpQGw/kGLQJV67Fr1D6k2OAIEgWodsSZGQJli2RFl/cXvS92pspusd9AlnE
cgdTX3Ry6etMwYqxM69992WUX9VFZqB3YUU2OytPo6OM+eMv1t2Ow5JmgdJR4fc8
Y0wNJnJRCh3lE8RAyczX3mMOe+8eHtQa3f/Zo1UiIPkQutDbtM7qDkEO96Rv8zXR
9fdF3zogIrc/NN35JpMz8QAQ8fhs2haksLr8uk5HuvUJNSCKCvt3rWkpe0t4Q4ro
wYQVle9v2sGxbywJDVOLGqEYnY+XWfacyWEsQ2BmChuaBVm/XpJWRIE3U4DE4qlr
L4Ijwx3gGlpwgsNWok3P4mA/PdfYuVrJbDALESaG0ck/lnGfl0JGJGDXDvrbeysr
iYuKBz7+FiFkzy7pMeI+tBPCpySTBkOjgRu18zrL7Xqrf0X+w13B07SwbdxhK2rz
umKK5v8N2iefJG18pW8rtw7/TTHQa5protQHxvxk/4O4i5jcdYiYkiwZhBn9nlUM
GC01k+CbMDYljmkvNDFEH5p672BxLkSFKnzu3VfmCiyVJQpj68oDhLuBpoA5e5cC
RqLVW95Xjp7FHXOiXUGnt3cujavlY2E6GKDJSwb2loaZR/6fhIsRL1Gy4W7849Hl
BV2vuCn7gYkmB2AbjbZnOXtXGlNepJjgVjTW0/72CArmwiiejDBJ3QQC1nE+Lyl8
fwHIB64U+dK4fxJrVX+qpfHZijWlDpR9YYSZTX7H6bpqd+/BwXloYYezGZpF+C/V
5Fe5nxlENm2bD67Yt0qHeZPBTDuPEuw5/M9dTMBLN5zxLkxtWctFm8y0QnHnag28
TP6ws8OrPY1//OHQuM/oL7tcZnd0B9K9n1TJPMUcNN3WT+uCkKeVSypEvfAmTVqT
cplhxkvCOzyiuUcB4Ul/4M1HYLO1IQwK7RiBLCscGpwg5qsOOi6s3r9IxQ27jd5U
Ls8UxWDZ1zdqwwPYRvBdSWwoN/Cpvgax3MEbktAblO6dVsCEXAzVOtFhEOz0/z9P
Haavs/3INBfXPh2IfHwHI+CTNzh/dK31tvvnUo6dffSneT9QGtANQSW22XOhgIRP
1QFKjQqZvnKO1lsEobTEMXhTNfyONAlKF65pYmPPfF1T2XcsXBNjmZsX9IWunRjt
Zy68FsuRy3rH5CF8FP+plGe+WtuhhnvqgtEMMWDEIp115OFXUu95fkbxegX5BXYI
KY5Ebx8n47W+8p31imJWsZ+HK86Y380YGMoNvgO4SAn4mVQ8qxcq9YQ3w0Bpw3a+
UGwfhW6vUfQWw3iXSRT8AcQFKgysX2o5PZ/j6GIkGO2SeUd5uvcFrqwmJW2+xxrT
lKRdXMUnPS5GUlATgg9ES5iX7+rVQ/DEdQmrKYnrKmwnpmjr6+7KzbzzsxnDHBo2
RNr1kr91daqc4ve0IGDvMbvxMEARk3M62qvD33xe2gFS7PCidcWRxFtZARdzw47m
sdkTrqeWukBlQHKHQ11Pd9w83cAhrJ1XTzzlcUIHiz0ckRHa+TapWkg5yBUrPHzr
xLOSjspCnwkksEs+vHlNJMAkJqqc9Lchd6q/Spobk1Zd/rhECrjex6DIlSDr1+us
XS74Pl9pyQy4TljWoMtILd9V5+Y5Riz3u0Q9PUWGtHGnxSbwlVMf3SHeXOZP4ME6
rhsKykjVZDMeI5TXNCULKjZ/hmNwL5Wg7qIMy5lK7hw6KI65rhmR4O1McLGt+AyY
B+/WS+zKyBxF6B3UBEsnv1i7O7rFUR18IcEg4fxs97On0bmoizS8Hog6adRl7Jdf
0Wz982rYyVoudlRmToLRqFt+3b0XQaSs349mAV5KZy+JN4Na39KyzfHmdgS1d+oX
+o+MhNQuJam6+ICGMv7F8+p1eFzYOZbPSVNMhHdsD3HreFJzJfjxRea7ocmhSJAY
am23Ilc61CIgP2EGmkZH/hP6RLCL/+2weSl5NZJTsmP+sNFwiPbbFq2cNwnuhzk4
12aut48nG7E5KVUjWj+MOtkVxTgwLqfQU+S7GCbroJ/OCKX5ewAATulXeL3u+evl
Fy66GE0EF0sjzvARtvFK2dF2VXPCdNRA3eHsbasAA25yKY0yQAQObMm4OFBimg9i
yX8CKLJc6tqX35SCe75/Y77n7CAyH+B6trAFE6z4n6nIsbHBUUp3Uz1xYofBe5mO
CUJN6LZ151Az4d/rv7xOBCciVktAZLSLLXRz/kwfiifI0/MNXDN43EFAV0HONJIb
gWGjPdKNUJZQFmrFO6XlCC4tJZQuUK57elhzNNkLg2QgKikVFGS5zx/6Cu421ycW
sx5f6T4qKzGX4rp92RmAqE3M6BzW7cqppUTKtd9cR87vaG/KabjQPZ2PG7FWomKB
mI/LQM644LVtc5ZWlxipRzuC5YHXYecPRctf9+kKya4iFuod6hIzb7ME6gHKrber
Em7yd0VoTVyiMBCt7e//pGdcp+40VYXD2Za2wVO/KS/YXIu/zgf/cCavKTPiIJps
0YlfDZ5RBPOUelbSpjxaQhtBo+ugUtK8MIgEt7iV4NDq0O9Z3S847r65pNichiKG
/Uvi2VVggfK+GFUO958SVBfGJT9cwZXuGqzKAWOHua7LNLUNsgvDxQrbm5vJIXh+
NrX90L75hH44eQ49j4eTM11k3Qv2M6RQaI/ed/lBz1y8MR9XxKi8fr2pSnNZCwor
ex0HouX4scztEd8s4mOeG+23Gv0tZpnZwfV9ks/NgG8hZ8RJCaG3yxWpp12Qau3Q
yN5gqRDgf3tVv93HuITdfGougNZA5iQvmDyJCBSqpxpaBUw3F0WnadBmSrW6tSi9
nYwTR6xf+sqCuuG1qH7uAlf6pjICr6B0XKL6sWtlM0LXgjY3HiPMXqjfV0YJTtqp
LeYnAik21+js0NpDr14PWJlROf/2I/ziUHrmcFT7hcJFmzQWPD47lWroo5ZsD0P2
lhjWmRvo8PppVCflY7V0+ClnZaaR+sk8pH4w27px5ySuEqAiYCSOz0hKP8XWu7lQ
eiHspvH8HDnpDvFZnVQ7vlhlQ3FhFdt7VAcArO4TzCOPwtaJqQwHLGJMhyYsWJN+
zyu+Pz6dnL8ArYZ+ATrpibnWlA6Y31Y96y452Z7BBXqaI3zg0S+IbxLpZqvdyx+A
aBy9GATxHoTEC2CVQbtues7NJqQSOkyVTzSAlsvv61a2mfR+/BZCOH2AXmtaAF8X
`pragma protect end_protected
